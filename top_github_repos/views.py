"""
Created by Danjie Wenren
"""

from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime, timedelta
from github import Github

g = Github("35639be28a860fe3330888e0ba33dbd890c69bef")


def index(request):
    """
    The front page where top 10 repos are listed. There can be two listed: one for
    historically mostly forked repo and one for the last 30 days

    These REST API calls should probably be on the client side and use js, but I
    have no experience in js and it would take too long for me to learn it for this
    programming assignment...

    """
    top_repo_historical = list(g.search_repositories('forks:>0 sort:forks')[0:10])
    past_date = (datetime.now() - timedelta(days=30)).date()
    top_repo_last_month = list(g.search_repositories('forks:>0 sort:forks created:>{}'.format(past_date.isoformat()))[0:10])
    return render(
        request, 'top_github_repos/list_page.html',
        {
            'top_repo_historical': top_repo_historical,
            'top_repo_last_month': top_repo_last_month
        }
    )


def trend(request, repo_id):
    """
    Trend data from https://developer.github.com/v3/repos/statistics/
    :param request:
    :param repo_id:
    :return:
    """
    repo = g.get_repo(repo_id)
    return render(
        request, 'top_github_repos/trend_page.html',
        {
            'repo_str': repo.owner.login+'/'+repo.name,
            'repo_commits': [commit_activity.raw_data for commit_activity in repo.get_stats_commit_activity()]
        }
    )
